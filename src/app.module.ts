import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SessionModule } from './session/session.module';
import { UserModule } from './user/user.module';
import { CourseModule } from './course/course.module';
import { FamilyRelationshipModule } from './family-relationship/family-relationship.module';

@Module({
	imports: [
		ConfigModule.forRoot({
			isGlobal: true,
			envFilePath: ['.env', '.env.heroku'],
		}),
		TypeOrmModule.forRoot({}),
		SessionModule,
		UserModule,
		CourseModule,
		FamilyRelationshipModule,
	],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {}
