import * as cookiesUtils from 'src/utils/cookies.utils';

export function cookieHttpOnlyExtractor(req) {
	let cookies = req.headers.cookie || '';
	return cookiesUtils.getCookie(cookies, 'access_token');
}
