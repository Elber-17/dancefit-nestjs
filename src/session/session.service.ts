import { HttpStatus, Injectable, Inject, CACHE_MANAGER } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { Response } from 'express';

import { Cache } from 'cache-manager';
import * as bcrypt from 'bcrypt';
import * as uniqid from 'uniqid';

import { User } from 'src/entities/user/user.entity';

@Injectable()
export class SessionService {
	constructor(
		@InjectRepository(User)
		private readonly userRepo: Repository<User>,
		private readonly jwtService: JwtService,
		private readonly configService: ConfigService,
		@Inject(CACHE_MANAGER) private cacheManager: Cache,
	) {}

	public async validateUser(email: string, password: string): Promise<any> {
		const user: User = await this.userRepo.findOne({
			where: { email: email },
		});

		if (user && bcrypt.compareSync(password, user.password)) {
			const { password, ...result } = user;
			return result;
		}

		return null;
	}

	public async login(user: any, response: Response): Promise<void> {
		response
			.status(HttpStatus.OK)
			.cookie(
				'access_token',
				this.jwtService.sign(user, {
					secret: this.configService.get('JWT_SECRET'),
					jwtid: uniqid(user.id),
					expiresIn: '30d',
				}),
				{ httpOnly: true, secure: true, sameSite: 'none' },
			)
			.json({ message: 'Usuario Logeado' });

		return;
	}

	public async logout(request: any, response: Response): Promise<void> {
		let jti: string = request.user.jti;

		await this.addTokenToBlackList(jti);

		response
			.status(HttpStatus.OK)
			.clearCookie('access_token')
			.json({ message: 'Session cerrada con éxito' });

		return;
	}

	private async addTokenToBlackList(jti: string) {
		await this.cacheManager.set(jti, jti, { ttl: 1000 });
	}

	public async check(response: Response) {
		response.status(HttpStatus.OK).json({
			message: 'Sesión Activa',
		});
	}
}
