import { Module, CacheModule } from '@nestjs/common';
import { SessionService } from './session.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

import * as redisStore from 'cache-manager-redis-store';

import { LocalStrategy } from './local.strategy';
import { JwtStrategy } from './jwt.strategy';
import { User } from 'src/entities/user/user.entity';

const fs = require('fs');
let redisConfig: {} = {};

let envPath = '';

if (fs.existsSync('./.env')) {
	envPath = './.env';
} else {
	envPath = './.env.heroku';
}

require('dotenv').config({ path: envPath });

if (process.env.REDIS_PASSWORD) {
	redisConfig = {
		store: redisStore,
		host: process.env.REDIS_HOST,
		port: process.env.REDIS_PORT,
		auth_pass: process.env.REDIS_PASSWORD,
	};
} else {
	redisConfig = {
		store: redisStore,
		host: process.env.REDIS_HOST,
		port: process.env.REDIS_PORT,
	};
}

@Module({
	imports: [
		TypeOrmModule.forFeature([User]),
		PassportModule,
		JwtModule.register({}),
		CacheModule.register(redisConfig),
	],
	providers: [SessionService, LocalStrategy, JwtStrategy],
	exports: [SessionService],
})
export class SessionModule {}
