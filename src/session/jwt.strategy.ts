import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';

import { cookieHttpOnlyExtractor } from './jwt.extractor';
import * as dotenv from 'dotenv';
import * as fs from 'fs';

let envPath = '';

if (fs.existsSync('./.env')) {
	envPath = './.env';
} else {
	envPath = './.env.heroku';
}

dotenv.config({ path: envPath });

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor() {
		super({
			jwtFromRequest: cookieHttpOnlyExtractor,
			ignoreExpiration: false,
			secretOrKey: process.env.JWT_SECRET,
		});
	}

	async validate(payload: any) {
		return payload;
	}
}
