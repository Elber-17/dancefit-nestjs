import { Injectable, ExecutionContext } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
	constructor() {
		super();
	}

	async canActivate(context: ExecutionContext): Promise<any> {
		let contextArgs: any = context.getArgs();

		return super.canActivate(context);
	}
}
