import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';

import { Course } from './course.entity';

@Entity()
export class CourseType {
	@PrimaryColumn({ type: 'smallint' })
	id: number;

	@Column('varchar', { length: 512 })
	type: string;

	@OneToMany(
		type => Course,
		course => course.type,
	)
	courses: Course[];
}
