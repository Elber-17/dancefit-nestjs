import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';

import { Course } from './course.entity';

@Entity()
export class CourseStatus {
	@PrimaryColumn({ type: 'smallint' })
	id: number;

	@Column('varchar', { length: 64 })
	code: string;

	@Column('varchar', { length: 256 })
	description: string;

	@OneToMany(
		type => Course,
		course => course.status,
	)
	courses: Course[];
}
