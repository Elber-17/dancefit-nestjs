import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	ManyToOne,
	JoinColumn,
} from 'typeorm';

import { Course } from './course.entity';

@Entity()
export class CourseImage {
	@PrimaryGeneratedColumn({ type: 'integer' })
	id: number;

	@Column('boolean')
	is_primary: boolean;

	@Column('varchar', { length: 2048 })
	path: string;

	@Column('integer')
	course_id: number;

	@ManyToOne(
		type => Course,
		course => course.id,
	)
	@JoinColumn({ name: 'course_id' })
	course: Course;
}
