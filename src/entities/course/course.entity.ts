import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	ManyToOne,
	DeleteDateColumn,
	JoinColumn,
	OneToMany,
} from 'typeorm';

import { CourseType } from 'src/entities/course/course-type.entity';
import { CourseStatus } from 'src/entities/course/course-status.entity';
import { CourseImage } from 'src/entities/course/course-image.entity';

@Entity()
export class Course {
	@PrimaryGeneratedColumn({ type: 'integer' })
	id: number;

	@Column('varchar', { length: 128 })
	title: string;

	@Column('varchar', { length: 512 })
	subtitle: string;

	@Column('smallint')
	type_id: number;

	@Column('smallint')
	status_id: number;

	@DeleteDateColumn()
	delete_at: Date;

	@ManyToOne(
		type => CourseStatus,
		courseStatus => courseStatus.id,
	)
	@JoinColumn({ name: 'status_id' })
	status: CourseStatus;

	@ManyToOne(
		type => CourseType,
		courseType => courseType.id,
	)
	@JoinColumn({ name: 'type_id' })
	type: CourseType;

	@OneToMany(
		type => CourseImage,
		courseImage => courseImage.course,
	)
	images: CourseImage[];
}
