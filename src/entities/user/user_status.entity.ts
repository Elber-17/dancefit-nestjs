import {
    Entity,
    Column,
    PrimaryColumn,
    OneToMany,
} from 'typeorm';

import { User } from 'src/entities/user/user.entity';

@Entity()
export class UserStatus {
    @PrimaryColumn({ type: 'smallint' })
    id: number;

    @Column('varchar', { length: 64 })
    code: string;

    @Column('varchar', { length: 256 })
    description: string;

    @OneToMany(
        type => User,
        user => user.status,
    )
    users: User[];
}

