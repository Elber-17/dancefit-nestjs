import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	ManyToOne,
	DeleteDateColumn,
	JoinColumn,
	OneToMany,
} from 'typeorm';

import { UserStatus } from 'src/entities/user/user_status.entity';
import { UserType } from 'src/entities/user/user_type.entity';
import { Responsable } from 'src/entities/responsable/responsable.entity';

@Entity()
export class User {
	@PrimaryGeneratedColumn({ type: 'bigint' })
	id: number;

	@Column('varchar', { length: 128 })
	name: string;

	@Column('varchar', { length: 128 })
	last_name: string;

	@Column('varchar', { length: 16 })
	cedula: string;

	@Column('varchar', { length: 16 })
	phone: string;

	@Column('varchar', { length: 128 })
	email: string;

	@Column('varchar', { length: 1024 })
	password: string;

	@DeleteDateColumn()
	delete_at: Date;

	@Column('smallint', { unsigned: true })
	status_id: number;

	@Column('smallint', { unsigned: true })
	type_id: number;

	@ManyToOne(
		type => UserStatus,
		status => status.id,
	)
	@JoinColumn({ name: 'status_id' })
	status: UserStatus;

	@ManyToOne(
		type => UserType,
		userType => userType.id,
	)
	@JoinColumn({ name: 'type_id' })
	type: UserStatus;

	@OneToMany(
		type => Responsable,
		responsable => responsable.user,
	)
	responsables: Responsable[];
}
