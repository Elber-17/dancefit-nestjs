import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	ManyToOne,
	JoinColumn,
} from 'typeorm';

import { User } from 'src/entities/user/user.entity';
import { FamilyRelationship } from 'src/entities/family-relationship/family-relationship.entity';

@Entity()
export class Responsable {
	@PrimaryGeneratedColumn({ type: 'bigint' })
	id: number;

	@Column('varchar', { length: 64 })
	name: string;

	@Column('varchar', { length: 64 })
	last_name: string;

	@Column('varchar', { length: 16 })
	phone: string;

	@Column('varchar', { length: 64 })
	email: string;

	@Column('bigint')
	user_id: number;

	@Column('bigint')
	family_relationship_id: number;

	@ManyToOne(
		type => User,
		user => user.id,
	)
	@JoinColumn({ name: 'user_id' })
	user: User;

	@ManyToOne(
		type => FamilyRelationship,
		familyRelationship => familyRelationship.id,
	)
	@JoinColumn({ name: 'family_relationship_id' })
	familyRelationship: FamilyRelationship;
}
