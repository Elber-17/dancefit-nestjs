import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

import { Responsable } from 'src/entities/responsable/responsable.entity';

@Entity()
export class FamilyRelationship {
	@PrimaryGeneratedColumn({ type: 'smallint' })
	id: number;

	@Column('varchar', { length: 64 })
	name: string;

	@OneToMany(
		type => Responsable,
		responsable => responsable.familyRelationship,
	)
	responsables: Responsable[];
}
