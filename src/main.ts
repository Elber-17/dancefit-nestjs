import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';

import { ConfigService } from '@nestjs/config';

import { getOrigins } from 'src/utils/origin.utils';

async function bootstrap() {
	const app = await NestFactory.create<NestExpressApplication>(AppModule);

	const myConfigService: ConfigService = app.get(ConfigService);

	app.enableCors({
		origin: await getOrigins(),
		credentials: true,
	});
	app.disable('x-powered-by');

	await app.listen(
		myConfigService.get('PORT') || 3002,
		myConfigService.get('HOST'),
	);
}
bootstrap();
