import {
	createParamDecorator,
	ExecutionContext,
	BadRequestException,
} from '@nestjs/common';
import { ObjectSchema } from 'joi';

import { ExampleSchema } from './schema.core';
export class CoreValidator {
	body: any;
	params: any;
	query: any;
	user: any;

	constructor(protected request: any) {
		this.getData();
	}

	private getData() {
		this.user = this.request.user;
		this.params = this.request.params;
		this.body = this.request.body;
		this.query = this.request.query;
	}

	// Este metodo es para sobreescribir
	public async validate(): Promise<any> {
		return {
			message:
				'Este es el resultado por default de la clase CoreValidation',
		};
	}

	protected async launchError(error): Promise<void> {
		if (Object.prototype.hasOwnProperty.call(error, 'details')) {
			switch (error.details[0].type) {
				case 'object.unknown':
					let key = error.details[0].context.key;
					throw new BadRequestException(
						`El parametro '${key}' no es un campo valido.`,
					);
			}
		}

		throw new BadRequestException(error.message);
	}

	protected async validateSchema(
		schema: ObjectSchema,
		data,
		array?: boolean,
	): Promise<void> {
		if (!array) {
			const { error } = schema.validate(data);

			if (error) {
				await this.launchError(error);
			}
		} else {
			for (let element of data) {
				const { error } = schema.validate(element);
				if (error) {
					await this.launchError(error);
				}
			}
		}

		return;
	}
}

//Ejemplo de uso de la clase

class ExampleValidator extends CoreValidator {
	constructor(protected request: any, private schema: ObjectSchema) {
		super(request);
	}

	public async validate() {
		await super.validateSchema(this.schema, this.query);
		return this.query;
	}
}

// Ejemplo del uso del guardia

export const ExampleValidatorGuard = createParamDecorator(
	(data: unknown, ctx: ExecutionContext) => {
		const request = ctx.switchToHttp().getRequest();
		const validation = new ExampleValidator(request, ExampleSchema);

		return validation.validate();
	},
);
