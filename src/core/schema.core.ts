// Esquema de ejemplo

import * as Joi from 'joi';

export function errorHandler(errors) {
	for (let error of errors) {
		if (error.code === 'any.required') {
			return new Error(`El campo ${error.local.label} es obligatorio`);
		} else if (error.code === 'number.base') {
			return new Error(
				`El campo ${error.local.label} debe ser un número`,
			);
		}
		return error;
	}
}

export const ExampleSchema = Joi.object({
	id: Joi.string()
		.required()
		.error(errors => {
			return errorHandler(errors);
		}),
});
