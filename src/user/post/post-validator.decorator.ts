import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { ObjectSchema } from 'joi';

import { CoreValidator } from 'src/core/validator.core';
import { UserBodyPostSchema } from './post-validator.schema';

class PostUserValidator extends CoreValidator {
	constructor(protected request: any, private schema: ObjectSchema) {
		super(request);
	}

	public async validate() {
		await super.validateSchema(this.schema, this.body);
		return this.body;
	}
}

export const PostUserValidatorGuard = createParamDecorator(
	(data: unknown, ctx: ExecutionContext) => {
		const request = ctx.switchToHttp().getRequest();
		const validation = new PostUserValidator(request, UserBodyPostSchema);

		return validation.validate();
	},
);
