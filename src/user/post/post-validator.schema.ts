// Esquema de ejemplo

import * as Joi from 'joi';
import { errorHandler } from 'src/core/schema.core';

export const UserBodyPostSchema = Joi.object({
	name: Joi.string().optional(),

	last_name: Joi.string().optional(),

	cedula: Joi.string().optional(),

	phone: Joi.string().optional(),

	email: Joi.string()
		.required()
		.error(errors => {
			return errorHandler(errors);
		}),

	password: Joi.string()
		.required()
		.error(errors => {
			return errorHandler(errors);
		}),
});
