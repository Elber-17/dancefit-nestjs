import {
	BadRequestException,
	HttpStatus,
	Injectable,
	InternalServerErrorException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Response } from 'express';
import { Connection, Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

import { User } from 'src/entities/user/user.entity';
import { Log } from 'src/utils/loger.utils';

@Injectable()
export class UserService {
	constructor(
		private connection: Connection,
		@InjectRepository(User) public readonly userRepo: Repository<User>,
	) {}

	public async getUser(userId: string, response: Response) {
		let {
			password,
			delete_at,
			status_id,
			type_id,
			...user
		} = await this.userRepo.findOne(userId);

		response.status(HttpStatus.OK).json(user);
	}

	public async createUser(
		userBodyRequest: {
			name?: string;
			last_name?: string;
			cedula?: string;
			phone?: string;
			email: string;
			password: string;
		},
		response: Response,
	) {
		let user: User = new User();
		Object.assign(user, userBodyRequest);

		user.status_id = 1;
		user.type_id = 3;
		user.password = user.password = bcrypt.hashSync(user.password, 12);

		let transactionResult: User = await this.createUserTransaction(user);
		let {
			password,
			delete_at,
			status_id,
			type_id,
			...finalUser
		} = transactionResult;

		response.status(HttpStatus.CREATED).json({
			message: 'Usuario creado con éxito',
			user: finalUser,
		});
	}

	private async createUserTransaction(user: User): Promise<User> {
		const queryRunner = this.connection.createQueryRunner();
		let transactionResult: User,
			error = null;

		await queryRunner.connect();
		await queryRunner.startTransaction();
		try {
			transactionResult = await queryRunner.manager.save(user);

			await queryRunner.commitTransaction();
		} catch (err) {
			error = err;
			await queryRunner.rollbackTransaction();
		} finally {
			await queryRunner.release();
		}

		if (error) {
			if (error.errno === 1062) {
				throw new BadRequestException(
					'Este correo Electrónico ya se encuentra registrado',
				);
			}

			Log(error);
			throw new InternalServerErrorException(
				'Ocurrio un error interno, intentalo más tarde (12)',
			);
		}

		return transactionResult;
	}
}
