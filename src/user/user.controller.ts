import { Controller, Get, Post, Req, Res, UseGuards } from '@nestjs/common';
import { Response } from 'express';

import { JwtAuthGuard } from 'src/session/jwt-auth.guard';
import { UserService } from './user.service';
import { PostUserValidatorGuard } from 'src/user/post/post-validator.decorator';

@Controller('user')
export class UserController {
	constructor(private service: UserService) {}

	@Get()
	@UseGuards(JwtAuthGuard)
	public async getUser(@Req() request, @Res() response: Response) {
		await this.service.getUser(request.user.id, response);
	}

	@Post()
	public async createUser(
		@PostUserValidatorGuard()
		userBodyRequest: {
			name?: string;
			last_name?: string;
			cedula?: string;
			phone?: string;
			email: string;
			password: string;
		},
		@Res() response: Response,
	) {
		await this.service.createUser(userBodyRequest, response);
	}
}
