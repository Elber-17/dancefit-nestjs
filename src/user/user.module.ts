import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserController } from './user.controller';
import { UserService } from './user.service';
import { User } from 'src/entities/user/user.entity';
import { SessionModule } from 'src/session/session.module';

@Module({
	imports: [TypeOrmModule.forFeature([User]), SessionModule],

	controllers: [UserController],
	providers: [UserService],
})
export class UserModule {}
