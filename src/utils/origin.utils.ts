import { Tedis } from 'tedis';
import * as dotenv from 'dotenv';

const fs = require('fs');

let envPath = '';

if (fs.existsSync('./.env')) {
	envPath = './.env';
} else {
	envPath = './.env.heroku';
}

dotenv.config({ path: envPath });

function getRedisClient() {
	let config: any = {
		port: parseInt(process.env.REDIS_PORT),
		host: process.env.REDIS_HOST,
	};
	process.env.REDIS_PASSWORD &&
		(config.password = process.env.REDIS_PASSWORD);

	const tedis = new Tedis(config);

	return tedis;
}

export async function getOrigins(): Promise<[any]> {
	let redisConnection = getRedisClient();
	let origins = await redisConnection.get('origins');

	origins = String(origins);

	console.log(JSON.parse(origins));

	return JSON.parse(origins);
}
