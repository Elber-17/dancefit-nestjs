import { Controller, Get, Res } from '@nestjs/common';
import { Response } from 'express';

import { CourseService } from './course.service';
@Controller('courses')
export class CourseController {
	constructor(private service: CourseService) {}

	@Get()
	public async getCourses(@Res() response: Response) {
		await this.service.getCourses(response);
	}
}
