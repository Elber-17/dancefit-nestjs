import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository } from 'typeorm';

import { ObjectSchema } from 'joi';

import { CoreValidator } from 'src/core/validator.core';
import { ImageParamGetSchema } from './get-validator.schema';
import { Course } from 'src/entities/course/course.entity';
import { CourseImage } from 'src/entities/course/course-image.entity';

class GetImageValidator extends CoreValidator {
	constructor(
		protected request: any,
		private schema: ObjectSchema,
		@InjectRepository(Course)
		private readonly courseRepo: Repository<Course>,
	) {
		super(request);
	}

	public async validate() {
		await super.validateSchema(this.schema, this.params);
		let courseImage: CourseImage = await this.validateCourse(
			this.params.courseId,
			this.params.imageId,
		);
		return courseImage;
	}

	private async validateCourse(
		courseId: string | number,
		imageId: string,
	): Promise<CourseImage> {
		let course: Course = await this.courseRepo.findOne(courseId, {
			relations: ['images'],
		});

		!course &&
			(await super.launchError({
				message: `No exite el curso con el id '${courseId}'`,
			}));

		return await this.validateImage(course.images, imageId);
	}

	private async validateImage(
		images: CourseImage[],
		imageId: string,
	): Promise<CourseImage> {
		for (let image of images) {
			if (image.id === parseInt(imageId)) {
				return image;
			}
		}

		await this.launchError({
			message: `No existe una image con el id '${imageId}'`,
		});
	}
}

export const GetImageValidatorGuard = createParamDecorator(
	(data: unknown, ctx: ExecutionContext) => {
		const request = ctx.switchToHttp().getRequest();
		const validation = new GetImageValidator(
			request,
			ImageParamGetSchema,
			getRepository(Course),
		);

		return validation.validate();
	},
);
