import * as Joi from 'joi';
import { errorHandler } from 'src/core/schema.core';

export const ImageParamGetSchema = Joi.object({
	courseId: Joi.number()
		.required()
		.error(errors => {
			return errorHandler(errors);
		}),

	imageId: Joi.number()
		.required()
		.error(errors => {
			return errorHandler(errors);
		}),
});
