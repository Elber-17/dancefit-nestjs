import { Controller, Get, Res } from '@nestjs/common';
import { GetImageValidatorGuard } from './get/get-validator.decorator';
import { Response } from 'express';

import { ImagesService } from './images.service';
import { CourseImage } from 'src/entities/course/course-image.entity';

@Controller('courses')
export class ImagesController {
	constructor(private readonly service: ImagesService) {}

	@Get(':courseId/image/:imageId')
	public async serveCourseImage(
		@GetImageValidatorGuard() courseImage: CourseImage,
		@Res() response: Response,
	) {
		await this.service.serveCourseImage(courseImage, response);
	}
}
