import { HttpStatus, Injectable } from '@nestjs/common';
import { Response } from 'express';

import { CourseImage } from 'src/entities/course/course-image.entity';

@Injectable()
export class ImagesService {
	public async serveCourseImage(
		courseImage: CourseImage,
		response: Response,
	) {
		response.contentType(
			'image/' + (courseImage.path.match(/\jpeg|png/g) || 'jpeg'),
		);

		response.sendFile(process.cwd() + courseImage.path);
	}
}
