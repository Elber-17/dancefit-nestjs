import { HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Course } from 'src/entities/course/course.entity';
import { Response } from 'express';

@Injectable()
export class CourseService {
	constructor(
		@InjectRepository(Course)
		public readonly courseRepo: Repository<Course>,
		private configService: ConfigService,
	) {}

	public async getCourses(response: Response) {
		let courses: Course[] = await this.courseRepo.find({
			select: ['id', 'title', 'subtitle'],
			relations: ['images'],
		});

		this.construcImageUrl(courses);

		response.status(HttpStatus.OK).json(courses);
	}

	private construcImageUrl(courses: Course[]) {
		for (let course of courses) {
			let imageResponse: any[] = [];

			for (let image of course.images) {
				let url =
					this.configService.get('BASE_URL') +
					'courses/' +
					course.id +
					'/image/' +
					image.id;

				imageResponse.push({ url: url, is_primary: image.is_primary });
			}

			course.images = imageResponse;
		}
	}
}
