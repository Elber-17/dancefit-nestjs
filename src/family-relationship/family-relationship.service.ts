import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Response } from 'express';

import { FamilyRelationship } from 'src/entities/family-relationship/family-relationship.entity';

@Injectable()
export class FamilyRelationshipService {
	constructor(
		@InjectRepository(FamilyRelationship)
		public readonly familyRelationshipRepo: Repository<FamilyRelationship>,
	) {}

	public async getFamilyRelationships(response: Response) {
		let familyRelationships: FamilyRelationship[] = await this.familyRelationshipRepo.find();

		response.status(HttpStatus.OK).json(familyRelationships);
	}
}
