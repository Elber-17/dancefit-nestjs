import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FamilyRelationshipController } from './family-relationship.controller';
import { FamilyRelationshipService } from './family-relationship.service';
import { FamilyRelationship } from 'src/entities/family-relationship/family-relationship.entity';

@Module({
	imports: [TypeOrmModule.forFeature([FamilyRelationship])],
	controllers: [FamilyRelationshipController],
	providers: [FamilyRelationshipService],
})
export class FamilyRelationshipModule {}
