import { Controller, Get, Res } from '@nestjs/common';
import { Response } from 'express';

import { FamilyRelationshipService } from './family-relationship.service';

@Controller('family-relationships')
export class FamilyRelationshipController {
	constructor(private readonly service: FamilyRelationshipService) {}

	@Get()
	public async getFamilyRelationships(@Res() response: Response) {
		await this.service.getFamilyRelationships(response);
	}
}
