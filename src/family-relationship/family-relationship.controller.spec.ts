import { Test, TestingModule } from '@nestjs/testing';
import { FamilyRelationshipController } from './family-relationship.controller';

describe('FamilyRelationshipController', () => {
  let controller: FamilyRelationshipController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FamilyRelationshipController],
    }).compile();

    controller = module.get<FamilyRelationshipController>(FamilyRelationshipController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
