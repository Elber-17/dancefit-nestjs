import { Test, TestingModule } from '@nestjs/testing';
import { FamilyRelationshipService } from './family-relationship.service';

describe('FamilyRelationshipService', () => {
  let service: FamilyRelationshipService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FamilyRelationshipService],
    }).compile();

    service = module.get<FamilyRelationshipService>(FamilyRelationshipService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
