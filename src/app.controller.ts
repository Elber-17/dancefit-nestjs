import {
	Controller,
	Get,
	UseGuards,
	Post,
	Request,
	Res,
	Delete,
} from '@nestjs/common';

import { AppService } from './app.service';
import { SessionService } from 'src/session/session.service';
import { LocalAuthGuard } from 'src/session/local-auth.guard';
import { JwtAuthGuard } from './session/jwt-auth.guard';
import { ExampleValidatorGuard } from './core/validator.core';

@Controller()
export class AppController {
	constructor(
		private readonly appService: AppService,
		private readonly sessionService: SessionService,
	) {}

	@Get()
	public getHello(): string {
		return this.appService.getHello();
	}

	@Get('test-validator')
	public testParam(@ExampleValidatorGuard() result: { id }): { id } {
		return result;
	}

	@UseGuards(JwtAuthGuard)
	@Get('protegida')
	public async protegida(@Request() request) {
		return { atencion: 'esta ruta está protegida' };
	}

	@UseGuards(LocalAuthGuard)
	@Post('session/login')
	public async login(@Request() req, @Res() response) {
		await this.sessionService.login(req.user, response);
	}

	@UseGuards(JwtAuthGuard)
	@Delete('session/logout')
	public async logout(@Request() request: any, @Res() response) {
		await this.sessionService.logout(request, response);
	}

	@UseGuards(JwtAuthGuard)
	@Get('session/check')
	public async check(@Res() response) {
		await this.sessionService.check(response);
	}
}
