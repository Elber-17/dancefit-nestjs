const fs = require('fs')

let envPath = '';

if (fs.existsSync('./.env')){
    console.log('***************\nEntorno: Local\n***************');
    envPath = './.env'
}
else{
    console.log('***************\nEntorno: Heroku\n***************');
    envPath = './.env.heroku'
}

require('dotenv').config({path:envPath});

module.exports = {
    "type": process.env.DB_TYPE,
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "username": process.env.DB_USER,
    "password": process.env.DB_PASSWORD,
    "database": process.env.DB_NAME,
    "entities": ["dist/**/*.entity{.ts,.js}"],
    "migrationsTableName": "custom_migration_table",
    "migrations": ["dist/migration/*{.ts,.js}"],
    "cli": {
        "migrationsDir": "migration"
    },
    "synchronize": false
}
