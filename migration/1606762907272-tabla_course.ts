import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class tablaCourse1606762907272 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'course',
				columns: [
					{
						name: 'id',
						type: 'integer',
						isPrimary: true,
						isGenerated: true,
						unsigned: true,
					},
					{
						name: 'title',
						type: 'varchar',
						length: '128',
					},
					{
						name: 'subtitle',
						type: 'varchar',
						length: '512',
					},
					{
						name: 'type_id',
						type: 'smallint',
						unsigned: true,
						isNullable: true,
					},
					{
						name: 'status_id',
						type: 'smallint',
						unsigned: true,
					},
					{
						name: 'delete_at',
						type: 'date',
						isNullable: true,
					},
				],
			}),
			true,
		);

		await queryRunner.createTable(
			new Table({
				name: 'course_type',
				columns: [
					{
						name: 'id',
						type: 'smallint',
						isPrimary: true,
						isGenerated: false,
						unsigned: true,
					},
					{
						name: 'type',
						type: 'varchar',
						length: '512',
					},
				],
			}),
			true,
		);

		await queryRunner.createTable(
			new Table({
				name: 'course_status',
				columns: [
					{
						name: 'id',
						type: 'smallint',
						isPrimary: true,
						isGenerated: false,
						unsigned: true,
					},
					{
						name: 'code',
						type: 'varchar',
						length: '64',
					},
					{
						name: 'description',
						type: 'varchar',
						length: '256',
					},
				],
			}),
			true,
		);

		await queryRunner.createTable(
			new Table({
				name: 'course_image',
				columns: [
					{
						name: 'id',
						type: 'integer',
						isPrimary: true,
						isGenerated: true,
						unsigned: true,
					},
					{
						name: 'is_primary',
						type: 'boolean',
						isNullable: true,
					},
					{
						name: 'path',
						type: 'varchar',
						length: '2048',
					},
					{
						name: 'course_id',
						type: 'integer',
						unsigned: true,
					},
				],
			}),
			true,
		);

		await queryRunner.createForeignKey(
			'course',
			new TableForeignKey({
				columnNames: ['type_id'],
				referencedColumnNames: ['id'],
				referencedTableName: 'course_type',
				onDelete: 'RESTRICT',
				onUpdate: 'CASCADE',
			}),
		);

		await queryRunner.createForeignKey(
			'course',
			new TableForeignKey({
				columnNames: ['status_id'],
				referencedColumnNames: ['id'],
				referencedTableName: 'course_status',
				onDelete: 'RESTRICT',
				onUpdate: 'CASCADE',
			}),
		);

		await queryRunner.createForeignKey(
			'course_image',
			new TableForeignKey({
				columnNames: ['course_id'],
				referencedColumnNames: ['id'],
				referencedTableName: 'course',
				onDelete: 'RESTRICT',
				onUpdate: 'CASCADE',
			}),
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
