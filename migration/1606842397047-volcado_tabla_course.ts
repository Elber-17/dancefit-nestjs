import { MigrationInterface, QueryRunner } from 'typeorm';

import { Course } from 'src/entities/course/course.entity';
import { CourseStatus } from 'src/entities/course/course-status.entity';
import { CourseType } from 'src/entities/course/course-type.entity';
import { CourseImage } from 'src/entities/course/course-image.entity';

export class volcadoTablaCourse1606842397047 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		let data: any = [
			{
				title: 'Gimmnasia',
				subtitle:
					'Práctica de ejercicio físico que sirve para desarrollar, fortalecer y dar flexibilidad al cuerpo..',
				pic: 'Gimnasia.jpg',
			},
			{
				title: 'Canto',
				subtitle:
					'Dominar la técnica vocal y proporcionarle al estudiante una visión amplia de los diferentes estilos musicales...',
				pic: 'Canto.jpg',
			},
			{
				title: 'COMMERCIAL DANCE',
				subtitle:
					'Tendencia que busca involucrar cualquier estilo de baile con la era en la que vivimos actualmente...',
				pic: 'Commercialdance.jpg',
			},
			{
				title: 'Danza',
				subtitle:
					'Elementos técnicos de la danza clásica y moderna, desarrollando el ritmo, dominio corporal, creatividad y concentración del niño...',
				pic: 'Danza.jpg',
			},
			{
				title: 'Lírico',
				subtitle:
					'Fusión de la danza contemporánea, el jazz y elementos de la gimnasia y el ballet.',
				pic: 'Lirico.jpg',
			},
			{
				title: 'Teatro',
				subtitle:
					'Desarrollar y potenciar las habilidades y destrezas corporales, gestuales y vocales...',
				pic: 'Teatro.jpg',
			},
		];

		await this.createStatuses(queryRunner);
		await this.createTypes(queryRunner);

		for (let elem of data) {
			let basePath: string = '/files/images/courses/';
			let courseEntity: Course = new Course();
			let courseImageEntity: CourseImage = new CourseImage();

			courseEntity.title = elem.title;
			courseEntity.subtitle = elem.subtitle;
			courseEntity.status_id = 1;
			courseEntity.type_id = 1;

			courseImageEntity.is_primary = true;
			courseImageEntity.path = basePath + elem.pic;
			courseImageEntity.course_id = (
				await queryRunner.manager.save(courseEntity)
			).id;

			await queryRunner.manager.save(courseImageEntity);
		}
	}

	private async createStatuses(queryRunner: QueryRunner) {
		let status1: CourseStatus = new CourseStatus();
		let status2: CourseStatus = new CourseStatus();
		let status3: CourseStatus = new CourseStatus();
		let status4: CourseStatus = new CourseStatus();
		let status5: CourseStatus = new CourseStatus();

		status1.id = 1;
		status1.code = 'Curso abierto, sin comenzar';
		status1.description =
			'Curso activo que aún no comienza, en el cual se pueden inscribir usuarios';

		status2.id = 2;
		status2.code = 'Curso en progreso, abierto';
		status2.description =
			'Curso activo en progreso, se pueden inscribir usuarios';

		status3.id = 3;
		status3.code = 'Curso en progreso, cerrado';
		status3.description =
			'Curso activo en progreso, no se pueden inscribir usuarios';

		status4.id = 4;
		status4.code = 'Curso Terminado';
		status4.description =
			'Curso Terminado, no se pueden inscribir usuarios';

		status5.id = 5;
		status5.code = 'Curso eliminado';
		status5.description = 'Curso eliminado';

		await queryRunner.manager.save([
			status1,
			status2,
			status3,
			status4,
			status5,
		]);
	}

	private async createTypes(queryRunner: QueryRunner) {
		let type1: CourseType = new CourseType();
		let type2: CourseType = new CourseType();

		type1.id = 1;
		type1.type = 'Free';

		type2.id = 2;
		type2.type = 'Premium';

		await queryRunner.manager.save([type1, type2]);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
