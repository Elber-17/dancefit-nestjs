import { truncate } from 'fs';
import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class tableResponsable1607278886785 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'responsable',
				columns: [
					{
						name: 'id',
						type: 'bigint',
						isPrimary: true,
						isGenerated: true,
						unsigned: true,
					},
					{
						name: 'name',
						type: 'varchar',
						length: '64',
					},
					{
						name: 'last_name',
						type: 'varchar',
						length: '64',
					},
					{
						name: 'phone',
						type: 'varchar',
						length: '16',
					},
					{
						name: 'email',
						type: 'varchar',
						length: '64',
					},
					{
						name: 'user_id',
						type: 'bigint',
						unsigned: true,
					},
					{
						name: 'family_relationship_id',
						type: 'bigint',
						unsigned: true,
					},
				],
			}),
			true,
		);

		await queryRunner.createTable(
			new Table({
				name: 'family_relationship',
				columns: [
					{
						name: 'id',
						type: 'smallint',
						isPrimary: true,
						isGenerated: true,
						unsigned: true,
					},
					{
						name: 'name',
						type: 'varchar',
						length: '64',
					},
				],
			}),
			true,
		);

		await queryRunner.createForeignKeys('responsable', [
			new TableForeignKey({
				columnNames: ['user_id'],
				referencedColumnNames: ['id'],
				referencedTableName: 'user',
				onDelete: 'RESTRICT',
				onUpdate: 'CASCADE',
			}),
			new TableForeignKey({
				columnNames: ['family_relationship_id'],
				referencedColumnNames: ['id'],
				referencedTableName: 'family_relationship',
				onDelete: 'RESTRICT',
				onUpdate: 'CASCADE',
			}),
		]);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
