import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class nuevosCamposTablaUserYCourse1607277504874
	implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumn(
			'user',
			new TableColumn({
				name: 'birthdate',
				type: 'date',
				isNullable: true,
			}),
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
