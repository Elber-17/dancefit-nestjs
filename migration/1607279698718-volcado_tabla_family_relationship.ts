import { MigrationInterface, QueryRunner } from 'typeorm';

import { FamilyRelationship } from 'src/entities/family-relationship/family-relationship.entity';

export class volcadoTablaFamilyRelationship1607279698718
	implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		let relationship1: FamilyRelationship = new FamilyRelationship(),
			relationship2: FamilyRelationship = new FamilyRelationship(),
			relationship3: FamilyRelationship = new FamilyRelationship(),
			relationship4: FamilyRelationship = new FamilyRelationship(),
			relationship5: FamilyRelationship = new FamilyRelationship(),
			relationship6: FamilyRelationship = new FamilyRelationship();

		relationship1.name = 'Padre';
		relationship2.name = 'Madre';
		relationship3.name = 'Tío';
		relationship4.name = 'Tía';
		relationship5.name = 'Abuelo';
		relationship6.name = 'Abuela';

		await queryRunner.manager.save([
			relationship1,
			relationship2,
			relationship3,
			relationship4,
			relationship5,
			relationship6,
		]);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
