import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

import { UserStatus } from 'src/entities/user/user_status.entity';
import { UserType } from 'src/entities/user/user_type.entity';

export class migracionInicial1605974057477 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await this.createTables(queryRunner);
		await this.createRelations(queryRunner);
		await this.dumpData(queryRunner);
	}

	private async createTables(queryRunner: QueryRunner) {
		await queryRunner.createTable(
			new Table({
				name: 'user',
				columns: [
					{
						name: 'id',
						type: 'bigint',
						isPrimary: true,
						isGenerated: true,
						unsigned: true,
					},
					{
						name: 'name',
						type: 'varchar',
						length: '128',
					},
					{
						name: 'last_name',
						type: 'varchar',
						length: '128',
						isNullable: true,
					},
					{
						name: 'cedula',
						type: 'varchar',
						length: '16',
						isNullable: true,
					},
					{
						name: 'phone',
						type: 'varchar',
						length: '16',
						isNullable: true,
					},
					{
						name: 'email',
						type: 'varchar',
						length: '128',
						isUnique: true,
					},
					{
						name: 'password',
						type: 'varchar',
						length: '1024',
					},
					{
						name: 'delete_at',
						type: 'date',
						isNullable: true,
					},
					{
						name: 'status_id',
						type: 'smallint',
						unsigned: true,
						isNullable: false,
					},
					{
						name: 'type_id',
						unsigned: true,
						type: 'smallint',
						isNullable: false,
					},
				],
			}),
			true,
		);
		await queryRunner.createTable(
			new Table({
				name: 'user_type',
				columns: [
					{
						name: 'id',
						type: 'smallint',
						isPrimary: true,
						isGenerated: false,
						unsigned: true,
					},
					{
						name: 'code',
						type: 'varchar',
						length: '64',
					},
					{
						name: 'description',
						type: 'varchar',
						length: '256',
					},
				],
			}),
			true,
		);
		await queryRunner.createTable(
			new Table({
				name: 'user_status',
				columns: [
					{
						name: 'id',
						type: 'smallint',
						isPrimary: true,
						isGenerated: false,
						unsigned: true,
					},
					{
						name: 'code',
						type: 'varchar',
						length: '64',
					},
					{
						name: 'description',
						type: 'varchar',
						length: '256',
					},
				],
			}),
			true,
		);
	}

	private async createRelations(queryRunner: QueryRunner) {
		await queryRunner.createForeignKeys('user', [
			new TableForeignKey({
				columnNames: ['status_id'],
				referencedColumnNames: ['id'],
				referencedTableName: 'user_status',
				onDelete: 'RESTRICT',
				onUpdate: 'CASCADE',
			}),
			new TableForeignKey({
				columnNames: ['type_id'],
				referencedColumnNames: ['id'],
				referencedTableName: 'user_type',
				onDelete: 'RESTRICT',
				onUpdate: 'CASCADE',
			}),
		]);
	}

	private async dumpData(queryRunner: QueryRunner) {
		let statusActive: UserStatus = new UserStatus();

		let typeAdmin: UserType = new UserType();
		let typeProfessor: UserType = new UserType();
		let typeStudent: UserType = new UserType();

		statusActive.id = 1;
		statusActive.code = 'Activo';
		statusActive.description =
			'Usuario activo y con permisos de usar la plataforma';

		typeAdmin.id = 1;
		typeAdmin.code = 'Administrador';
		typeAdmin.description =
			'Usuario Administrador de la plataforma, acceso a todos los modulos del sistema';

		typeProfessor.id = 2;
		typeProfessor.code = 'Profesor';
		typeProfessor.description =
			'Usuario registrado como profesor de la plataforma, con permisos de acceso al modulo de profesor';

		typeStudent.id = 3;
		typeStudent.code = 'Estudiante';
		typeStudent.description =
			'Usuario registrado como estudiante de la plataforma, solo puede acceder a cursos y comprarlos';

		await queryRunner.manager.save([
			typeAdmin,
			typeProfessor,
			typeStudent,
			statusActive,
		]);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
